var mongoose = require('mongoose');


var ProductSchema = new mongoose.Schema({
    date_added: String,
    type: String,
    sku: String,
    group_id: String,
    name: String,
    model: String,
    brand:String,
    url: String,
    image_url:String,
    on_special: Boolean,
    price: Number,
    flybuys_points: Number,
    shipping_class: String,
    add_to_cart: Boolean,
    categories: String,
    in_stock:  Boolean,
    sold_out:  Boolean
});

module.exports = mongoose.model('Product', ProductSchema);