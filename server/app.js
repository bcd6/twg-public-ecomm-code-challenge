const express = require('express');
const path = require('path');
const cookieParser = require('cookie-parser');
const logger = require('morgan');
const cors = require('cors');
const mongoose = require('mongoose');

const config = require('./config');
const jwt = require('./jwt');

const productController = require('./controllers/product.controller');

var app = express();

app.use(logger('dev'));
app.use(express.json());
app.use(express.urlencoded({ extended: false }));
app.use(cookieParser());
app.use(express.static(path.join(__dirname, 'public')));

app.use(cors());
// use JWT auth to secure the api
app.use('/api',jwt());
app.use('/api/product', productController.getAll);

app.get('*', function(req, res) {
  res.sendFile(path.join(__dirname, 'public')+'/index.html')
})

mongoose.connect(config.db, { useNewUrlParser: true });

var db = mongoose.connection;
db.on('error', console.error.bind(console, 'connection error:'));
db.once('open', function() {
  console.log("db start");
});

module.exports = app;
