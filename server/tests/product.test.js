
const chai = require('chai');
const chaiHttp = require('chai-http');
const app = require('../app');

chai.use(chaiHttp);
chai.should();

describe("Product", () => {
    describe("GET /", () => {
        it("should get all products record", (done) => {
             chai.request(app)
                 .get('/api/product')
                 .end((err, res) => {
                     res.should.have.status(200);
                     res.body.should.be.a('object');
                     done();
                  });
         });
    });
});
