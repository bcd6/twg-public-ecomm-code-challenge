﻿const express = require('express');
const product = require('../models/product');

let productController = {
    getAll: function (req, res, next) {
        let params = req.query;
        let category = params['c1'] ? params['c1'] : '';
        category = params['c2'] ? category + '\\|' + params['c2'] : category;
        category = params['c3'] ? category + '\\|' + params['c3'] : category;
        let pageIndex = params['pageIndex'];
        let keyword = params['keyword'];
        console.log(category);
        console.log(keyword);
        let categorieFilter = params['c3'] ? {
            categories: new RegExp("^" + category + "$")
        } : {
                categories: new RegExp("^" + category)
            };
        let nameFilter = params['keyword'] ? {
            name: new RegExp(keyword, 'i')
        } : {};
        let brandFilter = params['keyword'] ? {
            brand: new RegExp(keyword, 'i')
        } : {};

        product.count(categorieFilter)
            .or([nameFilter, brandFilter])
            .exec(function (err, count) {
                product.find(categorieFilter)
                    .or([nameFilter, brandFilter])
                    .sort({ 'name': -1 })
                    .limit(12)
                    .skip(12 * pageIndex)
                    .exec(function (err, data) {
                        console.log(count);
                        res.json({ count: count, data: data });
                    });
            });
    }
}

module.exports = productController;
