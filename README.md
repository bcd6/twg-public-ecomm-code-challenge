## How to start

1. Clone this repository.
2. cd client
3. run "npm install"
4. run "npm run build"
5. cd server
6. run "npm install"
7. Run "npm start"
8. Open http://localhost:3000/ in browser
