import { Component, OnDestroy } from '@angular/core';
import { BreakpointObserver, Breakpoints } from '@angular/cdk/layout';
import { NestedTreeControl } from '@angular/cdk/tree';
import { MatTreeNestedDataSource, MatExpansionPanel, MatSidenav } from '@angular/material';
import { ActivatedRoute, Router, NavigationEnd } from '@angular/router';
import { ViewportScroller } from '@angular/common';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent implements OnDestroy {
  sidnavMode: string;
  sidenavOpened: boolean;
  sidenavCategory = [
    {
      title: 'Computers & Tablets',
      value: 'Computers & Tablets',
      children: [

        {
          title: 'Laptops',
          value: 'Laptops',
          children: [
            {
              title: 'Macbooks',
              value: 'Macbooks'
            },
            {
              title: 'Laptop',
              value: 'Laptop'
            },
            {
              title: 'Gaming Laptops',
              value: 'Gaming Laptops'
            },
            {
              title: 'Chromebooks',
              value: 'Chromebooks'
            },
            {
              title: 'Modern PC',
              value: 'Modern PC'
            },
            {
              title: 'Premium Modern',
              value: 'Premium Modern'
            },
            {
              title: 'Laptop Bags & Accessories',
              value: 'Laptop Bags & Accessories'
            }
          ]
        },
        {
          title: 'Desktop Computers',
          value: 'Desktop Computers',
          children: [
            {
              title: ' PC Computers',
              value: ' PC Computers'
            },
            {
              title: 'Gaming Towers',
              value: 'Gaming Towers'
            },
            {
              title: 'iMacs',
              value: 'iMacs'
            },
            {
              title: 'Desktop Towers',
              value: 'Desktop Towers'
            }
          ]
        }
      ]
    },
    {
      title: 'Phones & GPS',
      value: 'Phones & GPS',
      children: [
        {
          title: 'Mobile Phones',
          value: 'Mobile Phones'
          // children: [
          //   {
          //     title: 'iPhone',
          //     value: 'iPhone'
          //   },
          //   {
          //     title: 'Android',
          //     value: 'Android'
          //   }
          // ]
        },
        {
          title: 'Home Phones',
          value: 'Home Phones'
        }
      ]
    },
    {
      title: 'Audio',
      value: 'Audio',
      children: [
        {
          title: 'Home Audio',
          value: 'Home Audio',
          children: [
            {
              title: 'Home Theatre Systems',
              value: 'Home Theatre Systems'
            },
            {
              title: 'Stereos',
              value: 'Stereos'
            },
            {
              title: 'Sound bars',
              value: 'Sound bars'
            },
            {
              title: 'Multi-room',
              value: 'Multi-room'
            },
            {
              title: 'Components',
              value: 'Components'
            },
            {
              title: 'Clock Radios',
              value: 'Clock Radios'
            }
          ]
        }
      ]
    },
    {
      title: 'Gift Card',
      value: 'GIFTCARDS'
    }
  ];
  c1;
  c2;
  c3;
  navigationSubscription;

  constructor(
    breakpointObserver: BreakpointObserver,
    private route: ActivatedRoute,
    private router: Router,
  ) {


    this.navigationSubscription = this.router.events.subscribe((e: any) => {
      // If it is a NavigationEnd event re-initalise the component
      if (e instanceof NavigationEnd) {
        this.initialise();
      }
    });


    const layoutChanges = breakpointObserver.observe([
      '(max-width: 599px)'
    ]);

    layoutChanges.subscribe(result => {
      if (result.matches) {
        this.sidnavMode = 'over';
        this.sidenavOpened = false;
      } else {
        this.sidnavMode = 'side';
        this.sidenavOpened = true;
      }
    });

  }

  initialise() {
    this.c1 = this.route.queryParams['value']['c1'];
    this.c2 = this.route.queryParams['value']['c2'];
    this.c3 = this.route.queryParams['value']['c3'];
    // this.keyword = this.route.queryParams['value']['keyword'];
  }


  ngOnDestroy() {
    if (this.navigationSubscription) {
      this.navigationSubscription.unsubscribe();
    }
  }


  go(c1 = '', c2 = '', c3 = '') {
    this.c1 = c1;
    this.c2 = c2;
    this.c3 = c3;
    this.router.navigate(['/product'], { queryParams: { c1: this.c1, c2: this.c2, c3: this.c3, keyword: '', pageIndex: 0 } });
  }
  goCart() {
    this.router.navigate(['/cart']);
  }
  sideNavToggle(sidenav: MatSidenav) {
    if (sidenav.mode === 'over') {
      sidenav.toggle();
    }
  }

  expandPanel(matExpansionPanel: MatExpansionPanel, sidenav: MatSidenav, event: Event): void {
    event.stopPropagation();
    if (!this._isExpansionIndicator(event.target)) {
      if (matExpansionPanel) {
        matExpansionPanel.toggle();
      }
      this.sideNavToggle(sidenav);
    }
  }

  private _isExpansionIndicator(target): boolean {
    const expansionIndicatorClass = 'mat-expansion-indicator';
    return (target.classList && target.classList.contains(expansionIndicatorClass));
  }
}
