import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { ProductComponent } from './components/product/product.component';
import { CartComponent } from './components/cart/cart.component';

const routes: Routes = [{
  path: '',
  redirectTo: '/product',
  pathMatch: 'full'
}, {
  path: 'product',
  component: ProductComponent,
  runGuardsAndResolvers: 'always',
}, {
  path: 'cart',
  component: CartComponent,
  runGuardsAndResolvers: 'always',
}, {
  path: '**',
  redirectTo: '/product'
}];

@NgModule({
  imports: [RouterModule.forRoot(routes, { onSameUrlNavigation: 'reload', scrollPositionRestoration: 'enabled' })],
  exports: [RouterModule]
})
export class AppRoutingModule { }
