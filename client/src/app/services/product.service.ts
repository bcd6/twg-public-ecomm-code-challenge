import { Injectable } from '@angular/core';
import { Observable, throwError } from 'rxjs';
import { HttpHeaders, HttpClient, HttpErrorResponse } from '@angular/common/http';
import { environment } from 'src/environments/environment';
import { map, catchError } from 'rxjs/operators';


const api = environment.api + '/api/product';

@Injectable({
  providedIn: 'root'
})
export class ProductService {

  private cart = [];
  private cartMap = {};

  constructor(private http: HttpClient) { }

  getCart() {
    return this.cart;
  }

  addToCart(obj) {
    let cartItem = {};
    if (!this.cartMap[obj._id]) {
      cartItem['name'] = obj.name;
      cartItem['price'] = obj.price;
      cartItem['quantum'] = 1;
      this.cartMap[obj._id] = cartItem;
    } else {
      cartItem = this.cartMap[obj._id];
      cartItem['quantum']++;
    }
    this.cart = Object.values(this.cartMap);
    console.log(this.cart);
  }

  get(params): Observable<any> {
    return this.http.get(api, { params: params }).pipe(
      map(this.extractData),
      catchError(this.handleError));
  }

  private handleError(error: HttpErrorResponse) {
    if (error.error instanceof ErrorEvent) {
      // A client-side or network error occurred. Handle it accordingly.
      console.error('An error occurred:', error.error.message);
    } else {
      // The backend returned an unsuccessful response code.
      // The response body may contain clues as to what went wrong,
      console.error(
        `Backend returned code ${error.status}, ` +
        `body was: ${error.error}`);
    }
    // return an observable with a user-facing error message
    return throwError('Something bad happened; please try again later.');
  }

  private extractData(res: Response) {
    const body = res;
    return body || {};
  }
}
