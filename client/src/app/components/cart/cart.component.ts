import { Component, OnInit } from '@angular/core';
import { ProductService } from 'src/app/services/product.service';

@Component({
  selector: 'app-cart',
  templateUrl: './cart.component.html',
  styleUrls: ['./cart.component.scss']
})
export class CartComponent implements OnInit {
  products;
  displayedColumns: string[] = ['name', 'quantum', 'price', 'total'];
  constructor(private productService: ProductService) {

  }

  ngOnInit() {
    this.products = this.productService.getCart();
    console.log(this.products.length);
  }

  getTotalCost() {
    return this.products.map(t => t.quantum * t.price).reduce((acc, value) => acc + value, 0);
  }
}
