import { Component, OnInit, OnDestroy } from '@angular/core';
import { ProductService } from 'src/app/services/product.service';
import { ActivatedRoute, Router, NavigationEnd } from '@angular/router';

@Component({
  selector: 'app-product',
  templateUrl: './product.component.html',
  styleUrls: ['./product.component.scss']
})
export class ProductComponent implements OnDestroy {
  products = [];
  navigationSubscription;
  count;
  params;
  loading = false;
  keyword;

  constructor(private productService: ProductService, private route: ActivatedRoute,
    private router: Router) {

    this.navigationSubscription = this.router.events.subscribe((e: any) => {
      // If it is a NavigationEnd event re-initalise the component
      if (e instanceof NavigationEnd) {
        this.initialise();
      }
    });
  }

  initialise() {
    this.loading = true;
    this.products = [];
    this.params = { ...this.route.queryParams['value'] };
    this.keyword = this.route.queryParams['value']['keyword'];
    console.log(this.params);
    this.productService.get(this.params).subscribe(response => {
      // console.log(response);
      this.products = response.data;
      this.count = response.count;
      this.loading = false;
    });
  }

  pageChange(e) {
    this.params['pageIndex'] = e.pageIndex;
    this.router.navigate(['/product'], { queryParams: this.params });
  }

  search() {
    this.params['keyword'] = this.keyword;
    this.params['pageIndex'] = 0;
    this.router.navigate(['/product'], { queryParams: this.params });
  }

  add(obj) {
    this.productService.addToCart(obj);
    console.log(this.productService.getCart().length);
  }

  ngOnDestroy() {
    // avoid memory leaks here by cleaning up after ourselves. If we
    // don't then we will continue to run our initialiseInvites()
    // method on every navigationEnd event.
    if (this.navigationSubscription) {
      this.navigationSubscription.unsubscribe();
    }
  }

}
